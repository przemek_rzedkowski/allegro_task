import { assert } from 'chai';
import { describe, it } from 'mocha';

const clientId = '911a63a98cc04d068fda803b961d392e';
const clientSecret = 'yRqnbLc8b8EK09K4y9ZvkLrS7kUBcv6VjvAWASYGNY98PK8tnNzKOJ7HbT23IZad';
const base64 = btoa(`${clientId}:${clientSecret}`);
let authToken;
const categoriesKeys = ['id', 'name', 'leaf', 'options', 'parent'];
const optionsKeys = [
	'advertisement',
	'advertisementPriceOptional',
	'customParametersEnabled',
	'offersWithProductPublicationEnabled',
	'productCreationEnabled',
	'variantsByColorPatternAllowed',
];
const parametersKeys = [
	'id',
	'name',
	'options',
	'required',
	'requiredForProduct',
	'restrictions',
	'type',
	'unit',
];

describe('Allegro API tests', () => {
	beforeEach(() => {
		cy.request({
			method: 'POST',
			url: 'https://allegro.pl/auth/oauth/token?grant_type=client_credentials',
			headers: {
				Authorization: `Basic ${base64}`,
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		}).then(response => {
			authToken = response.body.access_token;
		});
	});

	it('Get categories IDs', () => {
		cy.request({
			method: 'GET',
			url: 'https://api.allegro.pl/sale/categories',
			headers: {
				Authorization: 'Bearer ' + authToken,
				Accept: 'application/vnd.allegro.public.v1+json',
			},
		}).then(response => {
			const resBody = response.body;
			assert.equal(response.status, 200);
			assert.isArray(resBody.categories);
			cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.name)).then(categories => {
				resBody.categories.forEach(cat => {
					assert.oneOf(cat.name, categories);
				});
			});
			cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.id)).then(ids => {
				resBody.categories.forEach(cat => {
					assert.oneOf(cat.id, ids);
				});
			});
			assert.hasAllKeys(resBody.categories[0], categoriesKeys);
			assert.isObject(resBody.categories[0].options);
			assert.hasAllKeys(resBody.categories[0].options, optionsKeys);
			assert.isString(resBody.categories[0].id);
			assert.isString(resBody.categories[0].name);
			assert.isBoolean(resBody.categories[0].leaf);
			assert.isBoolean(resBody.categories[0].options.advertisement);
			assert.isBoolean(resBody.categories[0].options.advertisementPriceOptional);
			assert.isBoolean(resBody.categories[0].options.customParametersEnabled);
			assert.isBoolean(resBody.categories[0].options.offersWithProductPublicationEnabled);
			assert.isBoolean(resBody.categories[0].options.productCreationEnabled);
			assert.isBoolean(resBody.categories[0].options.variantsByColorPatternAllowed);
			assert.equal(resBody.categories.length, 13);
		});
	});

	it('Get a category by ID - response status OK', () => {
		cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.id)).then(ids => {
			ids.forEach(id => {
				cy.request({
					method: 'GET',
					url: 'https://api.allegro.pl/sale/categories/' + id,
					headers: {
						Authorization: 'Bearer ' + authToken,
						Accept: 'application/vnd.allegro.public.v1+json',
					},
				}).then(response => {
					assert.equal(response.status, 200);
				});
			});
		});
	});

	it('Get a category by ID - response status 404', () => {
		cy.request({
			method: 'GET',
			url: 'https://api.allegro.pl/sale/categories/' + 666,
			headers: {
				Authorization: 'Bearer ' + authToken,
				Accept: 'application/vnd.allegro.public.v1+json',
			},
			failOnStatusCode: false,
		}).then(response => {
			assert.equal(response.body.errors[0].message, "Category '666' not found");
			assert.equal(response.status, 404);
		});
	});

	it('Get a category by ID - response content', () => {
		cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.id)).then(ids => {
			ids.forEach(id => {
				cy.request({
					method: 'GET',
					url: 'https://api.allegro.pl/sale/categories/' + id,
					headers: {
						Authorization: 'Bearer ' + authToken,
						Accept: 'application/vnd.allegro.public.v1+json',
					},
				}).then(response => {
					cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.name)).then(categories => {
						assert.oneOf(response.body.name, categories);
					});
					assert.hasAllKeys(response.body, categoriesKeys);
					assert.hasAllKeys(response.body.options, optionsKeys);
				});
			});
		});
	});

	it('Get parameters supported by a category - status OK', () => {
		cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.id)).then(ids => {
			ids.forEach(id => {
				cy.request({
					method: 'GET',
					url: 'https://api.allegro.pl/sale/categories/' + id + '/parameters',
					headers: {
						Authorization: 'Bearer ' + authToken,
						Accept: 'application/vnd.allegro.public.v1+json',
					},
				}).then(response => {
					assert.equal(response.status, 200);
				});
			});
		});
	});

	it('Get parameters supported by a category - status 404', () => {
		cy.request({
			method: 'GET',
			url: 'https://api.allegro.pl/sale/categories/' + 666 + '/parameters',
			headers: {
				Authorization: 'Bearer ' + authToken,
				Accept: 'application/vnd.allegro.public.v1+json',
			},
			failOnStatusCode: false,
		}).then(response => {
			assert.equal(response.body.errors[0].message, "Category '666' not found");
			assert.equal(response.status, 404);
		});
	});

	it('Get parameters supported by a category - response content', () => {
		cy.fixture('categories.json').its('categories').then(categories => categories.map(c => c.id)).then(ids => {
			ids.forEach(id => {
				cy.request({
					method: 'GET',
					url: 'https://api.allegro.pl/sale/categories/' + id + '/parameters',
					headers: {
						Authorization: 'Bearer ' + authToken,
						Accept: 'application/vnd.allegro.public.v1+json',
					},
				}).then(response => {
					assert.isArray(response.body.parameters);
					if (response.body.parameters.length > 0) {
						assert.containsAllKeys(response.body.parameters[0], parametersKeys);
						assert.isObject(response.body.parameters[0].options);
						assert.isObject(response.body.parameters[0].restrictions);
					}
				});
			});
		});
	});
});
